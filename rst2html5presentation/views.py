from django.shortcuts import render
from django.http import HttpRequest , HttpResponse , HttpResponseRedirect
from django.template import loader
from django.core.urlresolvers import reverse
from django.core.files.storage import default_storage

import os


def index(request):
	return render(request, 'rst2html5presentation/index.html', {})

def watchPresentation(request):
	# initialization
	fileName = request.GET["presentationName"]
	author = request.GET["author"]
	userDirPath = "rst2html5presentation/Userdata/{}".format(author)
	if not os.path.exists(userDirPath):
		os.makedirs(userDirPath)
	presentationDirPath = "{}/{}".format(userDirPath,fileName)
	if not os.path.exists(presentationDirPath):
		os.makedirs(presentationDirPath)
	presentationStyle = request.GET["presentationStyle"]
	# write file
	text = request.GET["rstInput"]
	rstFile = open("{}/{}.rst".format(presentationDirPath,fileName), "w")
	rstFile.write(text)
	rstFile.close()
	#create presentation
	os.system('rst2html5 {} {}/{}.rst > {}/{}.html'.format(presentationStyle,presentationDirPath,fileName,presentationDirPath,fileName))
	# open presentation
	htmlFile = open("{}/{}.html".format(presentationDirPath,fileName), "r")
	htmlText = htmlFile.read()
	return HttpResponse(htmlText)

def uploadImagePage(request):
	presentationName = request.GET["presentationName"]
	author = request.GET["author"]
	return render(request, 'rst2html5presentation/uploadimagepage.html',{"presentationName":presentationName,"author":author})

def uploadImageResult(request):
	imageFile = request.FILES['imageFile']
	imageName = imageFile.name
	presentationName = request.POST["presentationName"]
	author = request.POST["author"]
	dirPath = 'rst2html5presentation/Userdata/{}/{}/images/'.format(author,presentationName)
	if not os.path.exists(dirPath):
		os.makedirs(dirPath)
	saveFilePath = default_storage.save(dirPath, imageFile)
	os.rename(saveFilePath,"{}{}".format(dirPath,imageName))
	imageNameWithOutType = imageName.split(".jpg")[0]
	return render(request, 'rst2html5presentation/uploadimageresultpage.html',{"presentationName":presentationName,"author":author,"imagename":imageNameWithOutType})

def getUploadImage(request,author,presentationname,imagename):
	imageDir = "rst2html5presentation/Userdata/{}/{}/images".format(author,presentationname)
	print(imageDir)
	if not os.path.exists(imageDir):
		os.makedirs(imageDir)
	with open("{}/{}.jpg".format(imageDir,imagename), "rb") as image:
		return HttpResponse(image.read(), content_type="image/jpeg")
