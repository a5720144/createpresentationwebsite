from django.apps import AppConfig


class Rst2Html5PresentationConfig(AppConfig):
    name = 'rst2html5presentation'
