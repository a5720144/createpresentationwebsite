from django.conf.urls import url

from . import views

app_name = 'rst2html5presentation'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^watchpresentation/$', views.watchPresentation, name='watchpresentation'),
    url(r'^watchpresentation/uploadimage/$', views.uploadImagePage, name='uploadimagepage'),
    url(r'^watchpresentation/uploadimage/result/$', views.uploadImageResult, name='uploadimageresult'),
    url(r'^watchpresentation/getuploadimage/(?P<author>\w+)/(?P<presentationname>\w+)/(?P<imagename>\w+)/$', views.getUploadImage, name='getuploadimage'),
]
