from django.conf.urls import include,url
from django.contrib import admin

urlpatterns = [
    url(r'^home/', include('rst2html5presentation.urls')),
    url(r'^$', include('rst2html5presentation.urls')),
    url(r'^admin/', admin.site.urls),
]
